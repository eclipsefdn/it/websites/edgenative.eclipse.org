---
title: Committee Minutes
hide_sidebar: true
---

The meeting minutes of the Eclipse Edge Native Working Group's steering committee.

{{< eclipsefdn_meeting_minutes yearly_sections_enabled="true" >}}